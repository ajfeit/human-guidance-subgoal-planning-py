import numpy
import math
from numpy import where, dstack, diff, meshgrid

def line_segment_intersect(xy1, xy2):

    n_rows_1, n_cols_1 = xy1.shape
    n_rows_2, n_cols_2 = xy2.shape

    sh = (n_rows_1, n_rows_2)

    if (n_cols_1 != 4) | (n_cols_2 != 4):
        print('Arguments must be a Nx4 matrices.')

    x1 = numpy.tile(xy1[:,0],(1,n_rows_2)).reshape(sh)
    x2 = numpy.tile(xy1[:,1],(1,n_rows_2)).reshape(sh)
    y1 = numpy.tile(xy1[:,2],(1,n_rows_2)).reshape(sh)
    y2 = numpy.tile(xy1[:,3],(1,n_rows_2)).reshape(sh)

    xy2 = xy2.T

    print(xy1.shape, xy2.shape)

    x3 = numpy.tile(xy2[0,:],(n_rows_1,1)) #.reshape(sh)
    x4 = numpy.tile(xy2[2,:],(n_rows_1,1)) #.reshape(sh)
    y3 = numpy.tile(xy2[1,:],(n_rows_1,1)) #.reshape(sh)
    y4 = numpy.tile(xy2[3,:],(n_rows_1,1)) #.reshape(sh)

    print(x1.shape, x2.shape, x3.shape, x4.shape)
    print(y1.shape, y2.shape, y3.shape, y4.shape)

    x4_x3 = x4 - x3
    y1_y3 = y1 - y3
    y4_y3 = y4 - y3
    x1_x3 = x1 - x3
    x2_x1 = x2 - x1
    y2_y1 = y2 - y1

    numerator_a = (x4_x3 * y1_y3) - (y4_y3 * x1_x3)
    numerator_b = (x2_x1 * y1_y3) - (y2_y1 * x1_x3)
    denominator = (y4_y3 * x2_x1) - (x4_x3 * y2_y1)

    u_a = numerator_a / denominator
    u_b = numerator_b / denominator

    int_x = x1 + (x2_x1 * u_a)
    int_y = y1 + (y2_y1 * u_a)
    int_b = (u_a >= 0) & (u_a <= 1) & (u_b >= 0) & (u_b <= 1)
    print(u_a.shape, u_b.shape, int_b.shape, x4_x3.shape, y1_y3.shape)
    par_b = (denominator == 0)
    coinc_b = (numerator_a == 0) & (numerator_b == 0) & (par_b)

    out = {}
    out['intAdjacencyMatrix'] = int_b
    out['intMatrixX'] = int_x * int_b
    out['intMatrixY'] = int_y * int_b

    return out


def steer_fs(x1, x2):
    x1 = x1[0:2].reshape([2,1])
    J = dist_fs(x1,x2)
    x = {}
    if not math.isinf(J):
        try:
            x['x'] = numpy.hstack([x1[0:2], x2[0:2]])
        except:
            print('---')
            print(x1, x2)
            print('----')
            raise
        d = x1[0:2] - x2[0:2]
        x['v'] = numpy.hstack([5*d/numpy.linalg.norm(d), 5*d/numpy.linalg.norm(d)])
        x['tt'] = numpy.hstack([-numpy.linalg.norm(d)/5.0, 0.0])
    else:
        x['x'] = []
        x['v'] = []
        x['tt'] = [numpy.inf]
    return x


def dist_fs(x1, x2):
    # print(x1, x2)
    x1 = x1.reshape([2,1])
    d = x2[0:2] - x1[0:2]

    psi2 = numpy.arctan2(x2[3],x2[2])
    psi1 = numpy.arctan2(d[1], d[0])
    # print(psi1, psi2)
    d_psi = numpy.absolute(psi2 - psi1)

    tt = numpy.linalg.norm(d)/5.0

    if d_psi > 5*(numpy.pi/180.0):
        tt = tt + 5*(d_psi/(numpy.pi/180.0))**2

    return tt


def is_between(line1, line2, pt):
    # parallel unit vector
    e11 = (line1[[0,2]] - line1[[1,3]])/numpy.linalg.norm(line1[[0,2]] - line1[[1,3]])
    e11 = e11.reshape([2,1])
    # perpendicular unit vector
    e12 = numpy.dot(numpy.array([[0,1],[-1,0]]), e11)
    # vector from point on line1 to point on line2
    s = line2[[0,2]].reshape([2,1]) - line1[[0,2]].reshape([2,1])
    # vector from point on line1 to point
    sa = pt - line1[[0,2]].reshape([1,2]).T
    d = numpy.dot(s.T, e12)[0,0]
    da = numpy.dot(sa.T, e12)[0,0]
    g1 = numpy.absolute(d) - numpy.absolute(da)
    between = (g1 > 0.1) & (numpy.sign(da) == numpy.sign(d))
    return between


def is_inside(pt, C, outer=False):
    obsnum = -1
    inside = False
    n = len(C)
    for i in range(n):
        if outer:
            xp = C[i]['sgp'][0,:]
            yp = C[0]['sgp'][1,:]
        else:
            xp = C[i]['verts'][0,:]
            yp = C[0]['verts'][1,:]

        line1 = numpy.array([xp[0], xp[1], yp[0], yp[1]])
        line2 = numpy.array([xp[2], xp[3], yp[2], yp[3]])
        line3 = numpy.array([xp[0], xp[3], yp[0], yp[3]])
        line4 = numpy.array([xp[1], xp[2], yp[1], yp[2]])

        ins = is_between(line1, line2, pt) & is_between(line3, line4, pt)
        if (ins):
            obsnum = i
        inside = inside | ins

    return inside, obsnum


def intersects(xp, C):
    # assemble list of all line segments in obstructions
    cl = []
    for i in range(len(C)):
        ls = numpy.hstack([C[i]['verts'][:,0].T, C[i]['verts'][:,1].T])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][:,1].T, C[i]['verts'][:,2].T])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][:,2].T, C[i]['verts'][:,3].T])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][:,3].T, C[i]['verts'][:,0].T])
        cl.append(ls)

    cl = numpy.array(cl)
    # then check to see if the trajectory intersects any of them
    # print(xp)
    xp = xp.T
    xln = numpy.hstack([xp[:-1,:], xp[1:,:]]).reshape([1,4])

    print(xln)
    print(cl)
    out = line_segment_intersect(cl, xln)
    #print(out)
    obs_int = out['intAdjacencyMatrix'].any(axis=0)
    print(out['intAdjacencyMatrix'])

    obs = numpy.ceil(numpy.where(obs_int)[0]/4)
    cst = obs_int.any()

    return cst, obs


def load_obstructions(fn_obs):
    print('loading obstructions')
    import scipy.io
    # fn_obs = '../../../Documents/research/data-analysis/data/Obstacles/newobs4.mat'
    mat = scipy.io.loadmat(fn_obs)
    obsdata = {}
    obsdata['num'] = mat['newobs4'][0][0][0][:,0]
    obsdata['xpos'] = mat['newobs4'][0][0][1][:,0]
    obsdata['ypos'] = mat['newobs4'][0][0][2][:,0]

    return obsdata


def load_config(fn_obs, skip):
    print('loading config')
    odata = numpy.genfromtxt(fn_obs, comments='%', delimiter='  ', 
                    skip_header=skip, max_rows=20, usecols=(0,1))
    cfg_data = {}
    cfg_data['sgx'] = odata[:,0]
    cfg_data['sgy'] = odata[:,1]
    return cfg_data


class RRTGraph:

    def __init__(self):
        pass

    
    def set_function(self, pt_func):
        self.pred_traj = pt_func

    def get_obs(self, obsdata):
        n = int(max(obsdata['num']))
        C = []
        for i in range(1,n):
            idx = numpy.where(obsdata['num'] == i)[0]
            ci = {}
            ci['verts'] = numpy.array([obsdata['xpos'][idx], obsdata['ypos'][idx]])
            ci['mean'] = numpy.array([[numpy.mean(ci['verts'][0,:])],[numpy.mean(ci['verts'][1,:])]])
            ci['sgp'] = []
            for j in range(len(ci['verts'])):
                vp = ci['verts'][:,j] - ci['mean']
                vpd = numpy.linalg.norm(vp)
                vpe = vp/vpd
                sgx = vpe * (vpd + self.sg_tol) + ci['mean']
                ci['sgp'].append(sgx)
            #ci['sgp'] = numpy.array(ci['sgp'])
            if len(ci['verts']) > 0:
                C.append(ci)
        self.C = C.copy()


    def sample_space(self):
        lims = numpy.vstack([self.xlim, self.ylim])
        npoint = numpy.random.uniform(lims[:, 0], lims[:, 1]).reshape(2,1)
        return npoint

    def run_rrt_star(self):
        vinit = {}
        vinit['pos'] = self.xgpos
        vinit['vel'] = self.xgvel
        V = [vinit]
        E = numpy.array([[numpy.nan]])
        self.vcost = [0]
        i = 0
        while (i <= self.k):
            self.V = V.copy()
            self.E = E.copy()
            pt = self.sample_space()
            self.ax.plot(pt[0], pt[1], 'r.')
            inside, obsnum = is_inside(pt[0:2], self.C)
            if not inside:
                zrand = {}
                zrand['pos'] = pt[0:2]
                if len(pt) > 2:
                    zrand['vel'] = pt[3:4]
                else:
                    zrand['vel'] = []
                i = i + 1
                V, E = self.extend(zrand)
                #print(V)
        zstart = {}
        zstart['pos'] = self.xstart
        zstart['vel'] = numpy.array([0, 0]).reshape([2,1])
        V, E = self.extend(zstart, True)
        self.V = V.copy()
        self.E = E.copy()


    def extend(self, z, isstart=False):
        Vp = self.V.copy()
        Ep = self.E.copy()
        znearest, znearesti, xnew = self.nearest(z)
        if len(xnew) > 0:
            znew = {}
            znew['pos'] = xnew['x'][:,0].reshape([2,1])
            znew['vel'] = self._vvf(xnew)
            out = intersects(xnew['x'], self.C)
            print(out)
            obs_free = (not out[0])
            print('free trajectory from ', znew['pos'], 'to', znearest['pos'], ':', obs_free)
            if obs_free:
                #print(znew)
                Vp.append(znew)
                znewi = len(Vp)-1
                cmin = self.vcost[znearesti] + self.J(xnew)
                self.vcost.append(cmin)
                zmin = znearest
                zmini = znearesti
                zni = self.nearvertices(znew, len(self.V))
                #print(zni)
                Znearby = [self.V[zi] for zi in zni]
                if len(Znearby) > 0:
                    for zneari,znear in enumerate(Znearby):
                        znewpt = numpy.vstack([znew['pos'].reshape([2,1]), znew['vel'].reshape([2,1])])
                        znearpt = numpy.vstack([znear['pos'].reshape([2,1]), znear['vel'].reshape([2,1])])
                        xnear = self.pred_traj(znewpt, znearpt)
                        if len(xnear['x']) > 0:
                            const = intersects(xnear['x'], self.C)
                            if not const:
                                if (self.vcost[zneari] + self.J(xnear)) < cmin:
                                    cmin = self.vcost[znewi] + self.J(xnear)
                                    self.vcost[znewi] = cmin
                                    zmin = znear
                                    zmini = zneari
                                    znew['vel'] = self._vvf(xnear)
                Ep = self.add_edge(Ep, znewi, zmini, cmin)
                Vp[znewi]['vel'] = znew['vel']
                if (len(Znearby) > 0) & (not isstart):
                    for zneari, znear in enumerate(Znearby):
                        znewpt = numpy.vstack([znew['pos'].reshape([2,1]), znew['vel'].reshape([2,1])])
                        znearpt = numpy.vstack([znear['pos'].reshape([2,1]), znear['vel'].reshape([2,1])])
                        xnear = self.pred_traj(znearpt, znewpt)
                        if len(xnear['x']) > 0:
                            const = intersects(xnear['x'], self.C)[0]
                            if (not const) & (self.vcost[zneari] > (self.vcost[znewi] + self.J(xnear))):
                                zparenti = self.get_parent(Ep, zneari)
                                newcost = self.J(xnear) + self.vcost[znewi]
                                Ep = self.change_edge(Ep, zneari, zparenti, znewi, newcost)
                                self.vcost[zneari] = newcost
                                Vp[zneari]['vel'] = self._vvf(xnear)
        return Vp, Ep


    def J(self, x):
        return -x['tt'][0]


    def _vvf(self, x):
        return x['v'][:,0]


    def get_parent(self, E, znodei):
        zrow = E[znodei,:]
        idx = numpy.argmin(zrow)
        return idx


    def change_edge(self, E, nodei, oldi, newi, newcost):
        Enew = E.copy()
        Enew[nodei, oldi] = numpy.inf
        Enew[nodei, newi] = newcost
        return Enew


    def add_edge(self, E, nodei, newi, newcost):
        Enew = E.copy()
        a = E
        b = numpy.full([E.shape[0],1], numpy.inf)
        c = numpy.full([1, E.shape[0]], numpy.inf)
        d = numpy.nan
        Enew = numpy.block([[a,b],[c,d]])
        Enew[nodei,newi] = newcost
        if nodei != (Enew.shape[0]-1):
            print('Graph index problem')
        return Enew


    def nearest(self, z):
        znearest = []
        ntraj = []
        mincost = numpy.inf
        zin = 0
        for i,zg in enumerate(self.V):
            traj = self.pred_traj(z['pos'].reshape([2,1]), numpy.vstack([zg['pos'].reshape([2,1]), zg['vel'].reshape([2,1])]))
            if (self.J(traj) < mincost) & (len(traj['x']) > 0):
                ntraj = traj
                znearest = zg
                mincost = self.J(traj)
                zin = i
        return znearest, zin, ntraj


    def nearvertices(self, z, n):
        if n==0:
            nmax = 0
            znidx = []
        else:
            nmax = 50 * numpy.log(n)/n
            nmax = min(max(10, nmax), n)
            nmax = numpy.floor(nmax)
            d = [self.J(self.pred_traj(z['pos'].reshape([2,1]), numpy.vstack([xi['pos'].reshape([2,1]), 
                                            xi['vel'].reshape([2,1])]))) for xi in self.V[1:]]
            #d = numpy.hstack([d, numpy.arange(len(d))])
            znidx = numpy.argsort(d)
        return znidx[-n:].astype(int)

    def compute_path(self):
        ds = [numpy.linalg.norm(xi['pos'] - self.xstart) for xi in self.V]
        idx = numpy.argmin(ds)
        y = ds[idx]
        self.path_list = []
        self.path_time = 0
        if y < 0.5:
            nxt = idx
            last = nxt
            while nxt != 0:
                self.path_list.append(numpy.vstack([self.V[nxt]['pos'].reshape([2,1]), self.V[nxt]['vel'].reshape([2,1])]))
                traj = self.pred_traj(self.V[last]['pos'].reshape([2,1]), numpy.vstack([self.V[nxt]['pos'].reshape([2,1]), self.V[nxt]['vel'].reshape([2,1])]))
                self.path_time += -traj['tt'][0]
                last = nxt
                idx = numpy.argmin(self.E[nxt,:])
                y = self.E[nxt,idx]
                if y < numpy.inf:
                    nxt = idx
                else:
                    nxt = 0
        else:
            self.path_time = numpy.nan
            self.path_list = []
            print('No path!')


    def plot_tree(self, ax):
        for i in range(len(self.V)):
            n = numpy.where((numpy.isfinite(self.E[:,i])))[0]
            print(n)
            if len(n) > 0:
                for j in range(len(n)):
                    traj = self.pred_traj(self.V[n[j]]['pos'].reshape(2,1), numpy.vstack([self.V[i]['pos'].reshape(2,1), self.V[i]['vel'].reshape(2,1)]))
                    ax.plot(traj['x'][0,:], traj['x'][1,:], 'b-')


    def plot_obstructions(self, ax):
        from matplotlib.patches import Polygon
        
        for i,ob in enumerate(self.C):
            print(ob['verts'].T)
            ax.add_patch(Polygon(ob['verts'].T))

        #if goal in obs:
        #    ax.add_patch(Polygon(obs['goal']))
