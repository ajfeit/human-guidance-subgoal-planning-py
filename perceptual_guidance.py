import numpy

def fp_global_guidance(x0, xg, guidparam):
    # % 1) convert x0 and xg into first person relevant goal information
    # % 2) run firstPersonGuidance
    # % 3) transform first-person results into global coordinates

    # note: non-turning-only not yet full implemented, only effects initial heading angle.
    turning_only = guidparam.get('turning_only', True)
    
    k = guidparam['k']
    
    # compute guidance geometry
    etheta = xg[0:2] - x0
    # print("xg, x0, etheta", xg, x0, etheta)
    beta_goal = numpy.arctan2(etheta[1], etheta[0])
    theta_goal = numpy.arctan2(xg[3], xg[2])
    thetaG = beta_goal - theta_goal
    if thetaG > numpy.pi:
        thetaG -= 2*numpy.pi
    elif thetaG < -numpy.pi:
        thetaG += numpy.pi
    flip = 0
    if thetaG < 0:
        thetaG = -thetaG
        flip = 1
    dG = numpy.linalg.norm(etheta)

    #  initial heading
    if (turning_only):
        psi0 = k*thetaG
    else:
        psi0 = 0.5 * k * thetaG

    # print("turning only:", turning_only, k*thetaG, psi0)
    
    # generate trajectory
    tj, pts, gparam = _first_person_turning_guidance(thetaG, psi0, dG, k, guidparam)

    # rotate
    R1 = numpy.array([[numpy.cos(thetaG), numpy.sin(thetaG)],[-numpy.sin(thetaG), numpy.cos(thetaG)]])
    # print("points", pts)
    pts1 = numpy.dot(pts[:,0:2],R1)
    thg = theta_goal - numpy.pi/2
    # flip if needed
    if flip:
        pts1[:,0] = -pts1[:,0]

    #rotate to correspond with global goal velocity
    R2 = numpy.array([[numpy.cos(thg), numpy.sin(thg)],[-numpy.sin(thg), numpy.cos(thg)]])
    pts2 = numpy.dot(pts1, R2)

    # transpose
    pts3 = pts2 + xg[0:2]
    traj = {}
    traj['x'] = pts3

    try:
        psiG = tj[:,0] + thetaG
    except:
        print(tj)

    if flip:
        psiG = -psiG

    psiG = psiG + theta_goal
    v1 = numpy.cos(psiG) * tj[:,1]
    v2 = numpy.sin(psiG) * tj[:,1]
    # print("velocities")
    # print(v1, v2)
    traj['v'] = numpy.vstack([v1, v2]).T
    traj['tt'] = pts[:,2].T - numpy.max(pts[:,2])
    traj['psi'] = psiG
    traj['modes'] = tj[:,3]

    return traj, gparam


def _first_person_turning_guidance(thetaG, psi0, dG, k, gparam):
    vmax = 2.0
    amax = 1.0
    aymax = 0.8
    dt = 0.02
    gparam['vmax'] = vmax
    gparam['k'] = k
    gparam['psi0'] = psi0
    turning_only = gparam.get('turning_only', True)

    # initial goal bearing error, positive to the right
    # theta_G = 15 * (pi/180);
    ethg = numpy.array([numpy.sin(thetaG), numpy.cos(thetaG)])

    # initial agent heading, positive to the left
    # psi = 5 * (pi/180);
    psi = psi0
    
    # unit vector in initial heading
    epsi = numpy.array([-numpy.sin(psi), numpy.cos(psi)])

    if psi > k*thetaG:
        print('Intial Heading Too High')
        psi = k*thetaG - 0.005

    # position of the vehicle relative to the goal
    xRG = numpy.array([[0, -dG]])

    # print("Initial vehicle relative position (dG, thetaG)", dG, thetaG)

    '''
    if pl
        % plot agent and goal
        figure(1)
        clf
        hold on
        plot([0 sin(theta_G)], [0 cos(theta_G)], 'k-', 'LineWidth', 2.0)
        % initia;, global position of agent
        plot([xRG(1) xRG(1)-sin(psi)], [xRG(2) xRG(2)+cos(psi)], 'k-', 'LineWidth', 2.0)
        axis equal
        xlim([-10 10])
        ylim([-41 5])
    end
    '''

    # Determine the point in front of the agent where steering should begin
    # first compute goal bearing error for initiating the turn:
    # psi = k * theta
    # theta_0 = psi / k

    # find position where turning starts
    # x(1) = d1 = distance in vehicle heading until turning starts
    Beta = (k*thetaG - psi) / (1 + k)
    if not turning_only:
        # d1*cos(psi) + d2*cos(Beta) = d_G
        # d1*sin(psi) - d2*sin(Beta) = 0
        # print("beta", Beta)
        if abs(Beta) < 0.001:
            x = numpy.array([[dG], [0]])
        else:
            A = numpy.array([[numpy.cos(psi), numpy.cos(Beta)], [numpy.sin(psi), -numpy.sin(Beta)]])
            b = numpy.array([[dG],[0]])
            # x = A \ b
            x = numpy.linalg.lstsq(A, b)[0]
            # print('braking point')
            # print(A, b, x)
        d0 = x[0,0]
    else:
        x = numpy.array([[dG],[0]])
        d0 = 0

    

    # point where turning begins, in goal-frame:
    #   initial point relative to the goal, plus distance to where
    #   turning begins, in the direction of initial heading.
    dm0 = xRG + d0*epsi
    gparam['dm0'] = dm0
    gparam['db0'] = dm0

    '''
    if pl
        %plot([xRG(1) dm0(1)], [xRG(2) dm0(2)], 'k-', 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5)
        %plot([dm0(1) 0], [dm0(2) 0], 'k-', 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5)
        %plot([0 -20*ethg(1)], [0 -20*ethg(2)], 'k-', 'Color', [0.7 0.7 0.7], 'LineWidth', 1.5)
        plot(dm0(1), dm0(2), 'k.', 'MarkerSize', 15.0)
    end
    '''

    # determine the point to begin braking
    # first determine the initial velocity for the turn element
    try:
        theta_dot = numpy.sqrt(((aymax / k) / x[1,0]) * numpy.sin(psi + Beta))
    except:
        print(x)
        raise
    psi_dot = k * theta_dot
    vlim = min(vmax, (aymax / psi_dot))
    gparam['vlim'] = vlim

    # rate of change of Tau

    # use max constant accel model
    db = (vmax**2 - vlim**2) / (2*amax)

    # if db is before the start point, then acceleration in the constraint
    if db > d0:
        # start braking immediately
        start = 2
        db = d0
        # then determine the max start vel
        tk = 0
        v0 = numpy.sqrt(vlim**2 + 2 * amax * db)
        traj = numpy.array([[psi, v0, 0.0, start]])
        db0 = xRG
        gparam['db0'] = 0
        gparam['v0'] = v0
    else:
        # we are in a trim element first
        v0 = vmax
        dtrim = d0 - db
        db0 = xRG + dtrim*epsi  # make sure dim is correct
        tk = dtrim / v0
        traj = numpy.array([[psi, v0, 0.0, 1],[psi, v0, tk, 1]])

    pts = numpy.hstack([xRG, numpy.array([[0]])])
    # simulate the braking segment
    vk = v0
    dk = db
    dvk = vk - vlim
    ak = amax
    dt = 0.02
    bdata = []

    while dk > 0:
        tk = tk + dt
        ak = amax
        dk = dk - vk * dt
        vk = vk - ak * dt
        bdata.append(numpy.array([ak, vk, dk, tk]))
        try:
            traj = numpy.vstack([traj, numpy.array([[psi, vk, tk, 2]])])
        except:
            print(traj, numpy.array([psi, vk, tk, 2]))
            raise

    bdata = numpy.array(bdata)
    # print('bdata')
    # print(bdata)
    # print(traj)
    # print()

    if len(bdata) > 0:
        bpts = numpy.array([db0[0,0] + bdata[:,2]*epsi[0], db0[0,1] + bdata[:,2]*epsi[1], bdata[:,3]]).T
        try:
            pts = numpy.vstack([pts, bpts])
        except:
            print(pts, bpts)
            raise
    # print("points after braking")
    # print(pts)

    # simulate the turning segment
    theta0 = thetaG # - Beta;
    # psi0 = psi; % + Beta;
    thetak = theta0
    psik = psi0
    # betak = Beta;
    # d = x(2);
    x0 = dm0 # -[sin(theta0) cos(theta0)]*d

    # dt = 0.02;
    tk = 0
    try:
        mpts = numpy.hstack([xRG.reshape(1,2), numpy.array([[tk]])]).reshape(1,-1)
        # print('mpts', mpts)
    except:
        print(x0, tk)
        print(dm0)
        print(xRG)
        print(d0)
        raise
    # pts = numpy.array([])

    betak = numpy.arctan2(-x0[0,0], -x0[0,1])
    d0 = numpy.linalg.norm(x0)
    theta_dot = numpy.sqrt((aymax / (k*d0)) * numpy.sin(psik+betak))
    psi_dot = (k*theta_dot)
    vtlim = (aymax / psi_dot)
    vk = min(vmax, vtlim)
    gparam['v0'] = vk

    # v0 = sqrt(vlim^2 + 2*amax*db);
    traj = numpy.array([[psi, vk, tk, 2]])

    # print(x0, dm0, xRG)
    while (numpy.linalg.norm(x0) > 0.25) & ((psik + betak) > 0.001):
        # print("looping:", x0, numpy.linalg.norm(x0))
        epsi = numpy.array([-numpy.sin(psik), numpy.cos(psik)])
        x0 = x0 + epsi*vk*dt
        tk = tk + dt
        #plot(x0(1), x0(2), 'k.')
        try:
            mpts = numpy.vstack([mpts, numpy.hstack([x0, numpy.array([[tk]]) ])])
        except:
            print("loop stacking error mpts:", mpts, numpy.hstack([x0, numpy.array([[tk]])]))
            raise
        betak = numpy.arctan2(-x0[0,0], -x0[0,1])
        thetak = thetaG - betak
        psik = k*thetak - betak
        
        d0 = numpy.linalg.norm(x0)
        theta_dot = numpy.sqrt((aymax / (k*d0)) * numpy.sin(psik+betak))
        psi_dot = (k*theta_dot)
        vtlim = (aymax / psi_dot)
        vk = min(vmax, vtlim)
        
        traj = numpy.vstack([traj, numpy.array([[psik, vk, tk, 3]])])
    
        
    try:
        mpts = numpy.vstack([mpts, numpy.array([[0, 0, tk+dt]])])
    except ValueError:
        print("mpts stacking error", mpts, numpy.array([[0, 0, tk+dt]]))
        raise
    '''
    if pl
        figure(1)
        hold on
        plot(mpts(:,1), mpts(:,2), 'b-', 'LineWidth', 2.0)
        plot_style(12)
        figure(3)
        clf
        plot(traj(:,3), traj(:,2), 'r-')
        hold on
        plot_style(12)
    end
    '''

    #traj
    

    #pts
    #mpts

    try:
        pts = numpy.vstack([pts, mpts])
        # pts = mpts
    except ValueError:
        print("value error", pts, mpts)
        raise

    # print("results, traj", traj)
    # print("results, pts", pts)

    return traj, pts, gparam
