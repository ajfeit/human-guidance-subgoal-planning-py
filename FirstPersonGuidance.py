import numpy
from numpy.linalg import norm
from numpy import arctan2
from numpy import hstack as hs
from numpy import vstack as vs
from numpy import array as ar
from numpy import cos, sin, tan

PI = numpy.pi

# function [ meas, btl, vobs ] = fp_cue_meas( x, obsdata )
def fp_cue_meas(x, obsdata):
    # compute a list of visible cue measurements given a vehicle state x and a
    # set of obstructions obsdata. optionally, plot the visible cues

    meas = {}

    if len(obsdata['num']) > 0:

        vpt = ar([[x[1]], [x[2]], [2.0]])
        vhd = x[2]

        # %rt = -vhd;
        # %C = [cos(rt) sin(rt); -sin(rt) cos(rt)];
        # %vobs = obs_from_data( obsdata, sg, vpt, vhd, xcen, ycen, obns );
        
        numbers = [2, 3, 4, 5, 6, 7]
        vobs = obs_from_data( obsdata, vpt, vhd, 0, 0, numbers )
        C = get_obs(obsdata)
        # print('vobs', vobs)

        sgp = numpy.vstack([ci['sgp'] for ci in C])
        vobs['sgp'] = sgp
        # print('sgp', sgp)

        # %compute the bearing transition list from set of visible obstructions
        (btl, vobs) = visible_cues_2(vobs)
        # print(vobs)

        # %btl;
        # %compute cue measurements
        (vbm, vec, hec, hml, hl) = cue_measurements( btl, vobs )

        meas['vbm'] = vbm  # % vertical bearing measurements
        meas['vec'] = vec  # % vertical edge cues
        meas['hec'] = hec  # % horizontal edge cues
        meas['hml'] = hml  # % horizontal measurement list?
        meas['hl'] = hl    # % horizontal lines?

    else:
        meas['vec'] = []
        meas['hl'] = []
        btl = []
        vobs = []

    return meas, btl, vobs


def obs_from_data( obsdata, vpt, vhd, xcen=0, ycen=0, obns=ar([2, 3, 4, 5, 6, 7]) ):
    vobs = {}
    obs = {}
    if (len(obsdata['num']) > 0):
        th = 0
        obs['ob'] = []
        obs['goal'] = []
        num_obs = numpy.max(obsdata['num'])
        rt = -th
        C = ar([
            [cos(rt), sin(rt)],
            [-sin(rt), cos(rt)]
        ])
        scale = 1.0
        xshft = 0
        yshft = 0
        for i in range(num_obs+1):
            # obs['ob'][i] = []
            obi = []
            for j in range(len(obsdata['num'])):
                if (obsdata['num'][j] == (i)):
                    # print(i,j, obsdata['num'][j], (i))
                    if (i == -1):
                        a = hs([obsdata['xpos'][j] - xcen - xshft, obsdata['ypos'][j] - ycen - yshft]).T
                        b = scale*numpy.dot(C, a).T
                        obs['goal'].append(b)
                    else:
                        a = hs([obsdata['xpos'][j] - xcen - xshft, obsdata['ypos'][j] - ycen - yshft]).T
                        b = scale*numpy.dot(C, a).T
                        obi.append(b)
            
            obs['ob'].append(vs(obi))
        # obs['goal'] = vs(obs['goal'])

    vobs['pts'] = []
    vobs['oid'] = []

    obsn = len(obs['ob'])
    ptl = {}
    for i in range(obsn):
        # print(i, obs['ob'][i])
        ptl = {}
        ptl['pos'] = obs['ob'][i]
        ptl['pid'] = vs([1, 2, 3, 4])
        ptl['oid'] = i
        vobs['pts'].append(ptl)
        vobs['oid'].append(i)
    vobs['oid'] = vs(vobs['oid'])

    vobs['vpt'] = vpt
    vobs['vhd'] = vhd
    return vobs

def get_obs(obsdata):
    sg_tol = 0.5
    n = int(max(obsdata['num'])) + 1
    C = []
    for i in range(0,n):
        idx = numpy.where(obsdata['num'] == i)[0]
        ci = {}
        ci['verts'] = numpy.array([obsdata['xpos'][idx], obsdata['ypos'][idx]]).T
        ci['mean'] = numpy.mean(ci['verts'], axis=0)
        # ci['mean'] = numpy.array([[numpy.mean(ci['verts'][0,:])],[numpy.mean(ci['verts'][1,:])]])
        ci['sgp'] = []
        for j in range(len(ci['verts'])):
            # print(ci['verts'][j,:])
            # print(ci['mean'])
            vp = ci['verts'][j,:] - ci['mean']
            vpd = numpy.linalg.norm(vp)
            vpe = vp/vpd
            sgx = vpe * (vpd + sg_tol) + ci['mean']
            # print("appending sg cand")
            # print(vp)
            # print(sgx)
            ci['sgp'].append(sgx)
        #ci['sgp'] = numpy.array(ci['sgp'])
        if len(ci['verts']) > 0:
            C.append(ci)
    return C

def visible_cues_2(vobs):
    # %   visible_cues determines the subset of cues which are visible from the
    # %   current vehicle state and visual field of view (fovx, fovy)
    # %  Point Cues:  determine if the cue point lies within the visual field.
    # %  Should also verify that cue is not blocked by other obstructions
    # %  Edge Cue:  Determine point on the edge within the current field of view
    # %  that provides the most information wrt estimating the angle of the edge
    # %  on the ground place.

    vobs['bel'] = []  # bearing extrema list
    for i,vpt in enumerate(vobs['pts']):
        # print(vpt)
        ths = []
        rs = []
        for j in range(4):
            rpt = (vpt['pos'][j,:] - vobs['vpt'][0:2,0]).reshape(-1)  # point relative to viewer 
            thi = arctan2(rpt[0], rpt[1]) - vobs['vhd']
            ri = norm(rpt)
            ths.append(thi)
            rs.append(ri)

        ths = numpy.vstack(ths)
        rs = numpy.vstack(rs)

        fovx = 30*(PI/180.0)*1.778
        a = (numpy.max(ths) < -fovx)

        if not (a or (numpy.min(ths) > fovx) or (numpy.max(ths) - numpy.min(ths) > 1.1*PI)):
            vobs['pts'][i]['th'] = ths
            vobs['pts'][i]['r'] = rs
            vobs['pts'][i]['visible'] = True

            # find bearing extrema
            bearings = numpy.hstack([ths, rs, vobs['pts'][i]['pid']])
            idx = numpy.argsort(ths)
            pc = bearings[idx,:]
            # print('pc', pc)
            vobs['pts'][i]['be'] = numpy.vstack([pc[0,:], pc[-1,:]])

            try:
                br = vobs['pts'][i]['be'][1,0] - vobs['pts'][i]['be'][0,0]
            except:
                print(vobs['pts'][i]['be'])
                print('shapes:')
                print(vobs['pts'][i]['be'].shape)
                raise

            if (br > PI):
                vobs['pts'][i]['be'] = numpy.vstack([
                        vobs['pts'][i]['be'][1,:], 
                        vobs['pts'][i]['be'][2,:]
                    ])
                vobs['pts'][i]['be'][1,0] = vobs['pts'][i]['be'][1,0] + 2*PI

            # determine if there is an intermediate near point on obstruction
            # points between extrema bearings that are closer than one or
            # more extrema

            # print(pc.shape)
            p1 = vobs['pts'][i]['pos'][vpt['be'][0,2].astype(int),:] - vobs['vpt'][0:2,0].T
            p2 = vobs['pts'][i]['pos'][vpt['be'][1,2].astype(int),:] - vobs['vpt'][0:2,0].T
            p3 = vobs['pts'][i]['pos'][pc[1,0,2].astype(int),:] - vobs['vpt'][0:2,0].T
            p4 = vobs['pts'][i]['pos'][pc[2,0,2].astype(int),:] - vobs['vpt'][0:2,0].T

            line2 = numpy.vstack([p1, p2])
            vec1 = numpy.vstack([numpy.zeros([1,2]), p3])
            vec2 = numpy.vstack([numpy.zeros([1,2]), p4])

            op1 = gaze_intersect_point_2(line2, vec1)
            op2 = gaze_intersect_point_2(line2, vec2)

            ips = numpy.vstack([op1['point'].T, op2['point'].T])

            nepi = pc[1:3,0,2].astype(int)  # non extrema point id
            nep = vobs['pts'][i]['pos'][nepi,:] - numpy.hstack([vobs['vpt'][0:2], vobs['vpt'][0:2]]).T
            dne = []
            dip = []
            
            for j in [0,1]:
                dne.append(norm(nep[j,:]))
                dip.append(norm(ips[j,:]))

            dne = ar(dne)
            dip = ar(dip)

            # print(dne, dip, dne-dip, ips, op1)
            dr = (dne - dip) < 0
            vobs['pts'][i]['vinpts'] = hs([nep[dr,:], nepi[dr].reshape(-1,1)])

            if dr.any():
                nedr = numpy.sqrt(nep[dr,0]**2 + nep[dr,1]**2)
                nepidx = numpy.ones(dr.shape)*i

                a = numpy.arctan2(nep[dr,0], nep[dr,1])
                # print(nedr, a)
                b = numpy.repmat(numpy.hstack([vobs['vhd'], 0, 0, 0]), numpy.sum(dr), 1)
                vobs['pts'][i]['vipb'] = numpy.hstack([a, nedr, nepi[dr], nepidx[dr]]) - b
            else:
                vobs['pts'][i]['vipb'] = []

            a = numpy.hstack([vobs['pts'][i]['be'], numpy.array([[i],[i]]), numpy.array([[1],[1]])])
            
            # vobs['bel'] = numpy.vstack([vobs['bel'], a])
            vobs['bel'].append(a)
        

        else:
            vobs['pts'][i]['th'] = ths
            vobs['pts'][i]['r'] = rs
            vobs['pts'][i]['visible'] = 0
    
    vobs['bel'] = vs(vobs['bel'])
    vobs['vpl'] = vobs['bel']

    for i in range(len(vobs['pts'])):
        if vobs['pts'][i]['visible']:
            if len(vobs['pts'][0]['vipb']) > 0:
                ln = len(vobs['pts'][i]['vipb'][:,0])
                a = numpy.hstack([vobs['pts'][0], numpy.zeros(ln,1)])
                vobs['vpl'] = numpy.vstack([vobs['vpl'], a])
    
    if len(vobs['bel'] > 0):
        # sort data
        idx = numpy.argsort(vobs['bel'][:,0])
        vobs['bel'] = vobs['bel'][idx,:]
        idx = numpy.argsort(vobs['bel'][:,1])
        vobs['del'] = vobs['bel'][idx,:]
        idx = numpy.argsort(vobs['vpl'][:,0])
        vobs['vpl'] = vobs['vpl'][idx,:]

        # print(vobs['del'].shape, vobs['bel'].shape)
        obsidx = ar([1, 2, 4, 3, 5])
        del_ = vobs['del'][obsidx,:]
        bel = vobs['bel'][obsidx,:]

    go = []
    btl = []
    cur = bel[0,:]
    btl = numpy.array([cur])
    go = numpy.array([cur[2]])
    for i in range(1,len(bel[:,1])):
        nxt = bel[i,:]
        if not (nxt[4] == 1):
            if len(go) > 0:
                btl = numpy.vstack([btl, nxt])
        elif (go == nxt[2]).any() & (nxt[4] == 1):
            ind = numpy.where(go == nxt[2])[0][0]
            if (ind == 0):
                # 'pop' the first obstruction
                go = go[1:]
                # go[0] is the index of the newly revealed obstruction
                # nxt[2] is the index of the obstruction that just ended
                # add the endpoint to the list of transitions
                btl = numpy.vstack([btl, nxt])
                # then start the next obstruction down in the queue
                if len(go) > 0: # if there is a next obstruction
                    # the next item in go is the just revealed obstruction
                    # first determine which intermediate points enclose LVP
                    fvpb = nxt[0]
                    if len(vobs['pts'][int(go[0])]['vipb'] > 0):
                        tp = vobs['pts'][go[0]]['vipb'][:,[0, 2]]
                        a = vobs['pts'][go[0]]['be'][0, [0, 2]]
                        c = vobs['pts'][go[0]]['be'][1, [0, 2]]
                        vpts = vs([a, tp, c])
                    
                    else:
                        tp = []
                        vpts = numpy.vstack([a, c])

                    j = numpy.where(vpts[:,0] < fvpb)[0][0]

                    # line between corners of newly revealed obstacles
                    pt1 = vobs['pts'][go[0]]['pos'][vpts[j,1],0:1] - vobs['pts'][0:1].T
                    pt2 = vobs['pts'][go[0]]['pos'][vpts[j+1,1],:] - vobs['pts'][0:1].T
                    line1 = numpy.vstack([pt1, pt2])

                    # line from agent in direction of bearing transition
                    pt3 = numpy.array([0, 0])
                    # see Matlab comments here
                    pt4 = vobs['pts'][nxt[2]]['pos'][nxt[3],0:1] - vobs['vpt'][0:1].T
                    line2 = numpy.vstack([pt3, pt4])
                    op = gaze_intersect_point_2(line1, line2)
                    a = numpy.hstack([ntx[0], norm(op['point']), go[0], vobs['pts'][go[0]]['pid'][j], 0])
                    btl = numpy.vstack([btl, a])
                else:
                    pass
                    # go[ind] = []

            else:  # start a new obstruction
                rnxt = nxt[1]
                if len(go) > 0:
                    rlst = mean(bel[bel[:,2]==go[0],1])
                else:
                    rlst = 1000

                if rnxt < rlst:
                    # push the start point to the top of the queue
                    go = numpy.vstack([nxt[2], go])
                    # end the current obstruction
                    if (len(go) > 1):
                        # if there is more than one obstruction in the queueu
                        # need to find the correct distance to the last visible point
                        # (LVP)
                        # on the last obstruction
                        lvpb = nxt[0]
                        # what? vvv
                        temp = vobs['pts'][go[1]]['vipb']
                        if len(temp) > 0:
                            temp = []
                        vpts = hs([
                                vobs['pts'][go[1]]['be'][0, [0, 2]], 
                                temp, 
                                vobs['pts'][go[1]]['be'][1, [0, 2]]
                            ])
                        j = 0
                        while (lvpb > vpts[j+1,0]):
                            j += 1
                        
                        obst_line = hs([
                            vobs['pts'][go[1]]['pos'][vpts[j,1],0:1] - vobs['vpt'][0:1].T,
                            vpbs['pts'][go[1]]['pos'][vpts[j+1],:] - vobs['vpt'][0:1].T,
                        ])
                        gaze_vec = hs([
                            ar([0, 0]),
                            vobs['pts'][go[0]]['pos'][nxt[3],0:1] - vobs['vpt'][0:1].T
                        ])
                        op = gaze_intersect_point_2(obst_line, gaze_vec)
                        btl = vs([
                            btl,
                            hs([nxt[0], norm(op['point']), go[1], nxt[3:4]])
                        ])

                    btl = vs([
                        btl,
                        nxt
                    ])

                else:
                    if (len(go) > 1):
                        tgo = vs([
                            go[1:],
                            nxt[2]
                        ])
                        tdo = []
                        for j in range(len(tgo)):
                            tdo.append(numpy.mean(bel[bel[:,2]==tgo[j],1]))
                        tdo = ar(tdo)
                        tgo = hs([tgo, tdo])
                        tgo = tgo[numpy.argsort(tgo[:,1]),:]
                        go = vs([
                            go[0],
                            tgo[:,0]
                        ])
                    else:
                        go = vs([
                            go,
                            nxt[2]
                        ])
        
        # check left field of view boundary
        def interp_point(btl, il):
            x = numpy.multiply(btl[il:il+1,1], numpy.sin(btl[il:il+1,0]))
            y = numpy.multiply(btl[il:il+1,1], numpy.cos(btl[il:il+1,0]))
            vec = vs([
                hs([0, 0]),
                hs([10.0*numpy.sin(-fovx), 10.0*numpy.cos(-fovx)])
            ])
            op = gaze_intersect_point_2(hs([x, y]), vec)
            r = norm(op['point'])
            th = arctan2(op['point'][0], op['point'][1])
            fvt = hs([th, r, btl[il,2:3], 0])
            return fvt
        
        fovx = 30.0 * 1.778 * (numpy.pi / 180.0)
        
        # print('btl', btl)
        if len(btl) > 0:
            il = numpy.where(((btl[1:,0] > -fovx) & (btl[0:-1,0] < -fovx)))[0]
        else:
            il = []
        
        
        if len(il) > 0:
            if (btl[il,2] == btl[il+1,2]):
                # then interpolate a point between them
                interp_point(btl, il)
            else:
                fvt = []
            if len(fvt) > 0:
                btl = hs([
                    fvt,
                    btl[il+1:,:]
                ])
        
        # check right field of view boundary
        if len(btl) > 0:
            il = numpy.where(((btl[1:,0] > fovx) & (btl[0:-1,0] < fovx)))[0]
        else:
            il = []
        if len(il) > 0:
            if (btl[il,2] == btl[il+1,2]):
                # then interpolate a point between them
                interp_point(btl, il)
            else:
                fvt = []
            if len(fvt) > 0:
                btl = hs([
                    btl[il+1:,:],
                    fvt
                ])
            else:
                btl = btl[0:il,:]
        
        # else:
        #     btl = []

    return btl, vobs


def cue_measurements( btl, vobs ):
    if len(btl) > 0:
        nobs = len(vobs['pts'])
        # compute bearing to vertical edge cues
        # only use edge cues that are not near the edge of the visual field
        cc = btl[(numpy.absolute(btl[:,0]) < 0.9309) | (btl[:,3] == 1),0:4]
        veb = numpy.unique(cc[:,0])
        vec = []
        vbm = numpy.ones([1,nobs*4])*numpy.nan
        for i in range(len(veb)):
            vb = cc[cc[:,0] == veb[i],0:4]
            if len(vb) > 0:
                vb = vb[numpy.argsort(vb[:,1]),:]
                vb = vb[0,:]
                vec.append(vb)
        vec = vs(vec)
        if len(vec)>0:
            for i in range(len(vec[:,0])):
                vbm[0,int((4*(vec[i,2] - 1) + vec[i,3]))] = vec[i,0]
        
        # compute horizontal edge cues
        # perpendicular distance from center in visual field of view
        # orientation in visual field of view
        # left and right endpoint bearings

        # line across the bottom of the field of view
        bfv = ar([[0, 0], [960, 0]])

        hec = []
        hml = {}
        hml['d'] = numpy.ones([1,nobs*4])*numpy.nan
        hml['phi'] = numpy.ones([1,nobs*4])*numpy.nan
        hml['th1'] = numpy.zeros([1,nobs*4])
        hml['th2'] = numpy.zeros([1,nobs*4])
        
        n = len(vobs['pts'])

        print(btl)

        for i in range(n):
            
            # get bearing transition list line corresponding to the current obstruction 
            v = btl[btl[:,2]==i,:]
            thv = v[:,0]
            rv = v[:,1]
            x = numpy.multiply(rv, numpy.sin(thv))
            y = numpy.multiply(rv, numpy.cos(thv))
            id = (v[:,2] - 1)*4 + v[:,3] + nobs*4
            print('pt', i, len(x))
            # determine visibility of the horizontal edge cue
            for j in range(len(x)-1):
                print(' idx', j)
                pt1 = vs([
                    x[j],
                    y[j],
                    -0.5
                ])
                pt2 = vs([
                    x[j+1],
                    y[j+1],
                    -0.5
                ])

                pt1_fv = sceneTofp(pt1, 0, [960, 540])
                pt2_fv = sceneTofp(pt2, 0, [960, 540])
                print(x, y)
                print(pt1_fv, pt2_fv)
                # if this cue is not totally outside the field of view
                if (pt1_fv[1] > 0) | (pt2_fv[1] > 0):
                    # if one endpoint is outside
                    if (pt1_fv[1] < 0) | (pt1_fv[1] < 0):
                        # find the intersection of the cue line with the edge of the fov
                        pts_fv = hs([
                            pt1_fv,
                            pt2_fv
                        ])
                        vpt_fv = pts_fv[argsort(-pts_fv[:,1]),:]
                        vpt_fv = pts_fv[0,:]
                        op = gaze_intersect_point_2(bfv, pts_fv)
                        pts_fv = vs([
                            op['point'].T,
                            vpt_fv
                        ])
                        pts_fv = pts_fv[numpy.argsort(pts_fv[:,0]),:]
                        pt1_fv = pts_fv[0,:]
                        pt2_fv = pts_fv[1,:]

                    e1 = pt2_fv - pt1_fv
                    phi = arctan2(e1[1], e1[0])
                    e2 = vs([
                        numpy.cos(phi + numpy.pi/2.0),
                        numpy.sin(phi + numpy.pi/2.0)
                    ])
                    d = numpy.dot((pt1_fv - ar([480, 270])), e2)

                    print('e1', e1, 'e2', e2, phi, d)

                    idx = int((v[j,2]-1)*4 + v[j,3])
                    hml['d'][0,idx] = d
                    hml['phi'][0,idx] = phi
                    hml['th1'][0,idx] = thv[j]
                    hml['th2'][0,idx] = thv[j+1]

                    a = hs([
                        thv[j:j+2].reshape(2,1), 
                        rv[j:j+2].reshape(2,1), 
                        x[j:j+2].reshape(2,1), 
                        y[j:j+2].reshape(2,1), 
                        id[j:j+2].reshape(2,1)
                        ])
                    hec.append(a)
        # hec = vs(hec)
        hl = []
        print(hec)
        for i in range(len(hec)):
            for j in range(len(hec[0])-1):
                hl.append(hs([hec[i][j,2:3], hec[i][j+1,2:3]]))
        
    else:
        vbm = []
        vec = []
        hec = []
        hml = []
        hl = []

    return vbm, vec, hec, hml, hl

def gaze_intersect_point_2(line2, vec):
    # % Determine the point where a gaze vector from the vehicle state to a gaze_point
    # % intersects the cue

    # %lines in the form:
    # % [x1 y1; x2 y2]

    # % line1 extends from the vehicle position to the gaze point
    # %line1 = [state(1:2) (state(1:2) + (gaze_point - state(1:2)*10))]'
    # line1 = vec;

    # % line2 connects the endpoints of the cue edge

    line1 = vec
    slope = lambda line: (line[1,1] - line[0,1])/(line[1,0] - line[0,0])
    def rec_slope (line): 
        try:
            a = (line[1,0] - line[0,0])/(line[1,1] - line[0,1])
        except:
            print(line, line.shape)
            raise
        return a
    
    def intercept(line, m): 
        return line[0,1] - m*line[0,0]
    
    is_point_inside = lambda xint, line: (
        ((xint > line[0,0]) & (xint < line[1,0])) |
        ((xint > line[1,0]) & (xint < line[0,0]))
    )

    theta = 7.0*numpy.pi/180.0

    R = ar([
            [numpy.cos(theta), -numpy.sin(theta)],
            [numpy.sin(theta), numpy.cos(theta)]
        ])

    i = 0
    # rotate the lines so they are not near vertical
    while((rec_slope(line1) == 0) | (rec_slope(line2) == 0)):
        line1 = numpy.dot(line1, R)
        line2 = numpy.dot(line2, R)
        i += 1

    m1 = slope(line1)
    m2 = slope(line2)
    b1 = intercept(line1, m1)
    b2 = intercept(line2, m2)

    xintersect = (b2 - b1) / (m1 - m2)
    yintersect = (m1 * xintersect) + b1

    inside = is_point_inside(xintersect, line2)
    behind = is_point_inside(xintersect, line1)

    point = ar([
        [xintersect],
        [yintersect]
    ])

    point = numpy.dot(numpy.linalg.matrix_power(R,i), point)

    output = {
        'exists': inside,
        'behind': behind,
        'point': point
    }
    
    return output

def sceneTofp( x_scene, agentheading, res):
    # Note: update this to use camera intrinsics/extrinsincs
    xres = res[0]
    yres = res[1]
    
    fov = 30*(numpy.pi/180)
    a = 1.0/numpy.tan(fov*1.778)

    x = x_scene[0]
    y = x_scene[1]
    z = x_scene[2]

    theta = arctan2(x, y) - agentheading
    nxy = numpy.sqrt((x**2) + (y**2))
    phi = arctan2(z, nxy)
    xpos1 = numpy.floor((a * numpy.tan(theta)) * (xres/2.0) + (xres/2))
    b = 1.0/numpy.tan(numpy.cos(theta)*fov/1.778)
    ypos1 = numpy.floor((b * numpy.tan(phi)) * (yres/2.0) + (yres/2.0))

    x_fp = hs([xpos1, ypos1])

    return x_fp

def getFPSubgoals(meas, vobs, xg, x, thR):

    if len(meas['vec']) > 0:
        sgfp = meas['vec']
        for i in range(len(sgfp[:,0])):
            # 1) determine the obstacle number that a point belongs to
            if len(vobs['bel']) > 0:
                oi = vobs['bel'][numpy.where(vobs['bel'][:,0]==sgfp[i,0])[0],3].astype(int)
            # 2) determine if this point is at the beginning or end of that obstacle
            if len(oi) > 0:
                ib = find(vobs['pts'][oi]['be'] == sgfp[i,0])

            # 3) based on this add or subtract an angle, epsilon
            if ib==1:
                sgfp[i,0] += -1.0/sgfp[i,1]
            else:
                sgfp[i,0] += 1.0/sgfp[i,1]

    else:
        sgfp = []
    
    # add the ultimate goal as a subgoal in the first-person view
    dg = xg['pos'] - x[0:2]
    gd = norm(dg)
    gth = arctan2(dg[0], dg[1])

    R = ar([[cos(thR), sin(thR)],[-sin(thR), cos(thR)]])
    hlG = []
    if len(meas['hl']) > 0:
        for i in range(len(meas['hl'][:,0])):
            hl = vs([meas['hl'][i,0:2], meas['hl'][i,2:4]]) * R.T
            hlG.append(hs([hl[0,:] + x[0:2].T, hl[1,:] + x[1:2].T]))

    hlG = numpy.array(hlG)

    return sgfp, hlG

def find(condition):
    return numpy.where(condition)[0]
