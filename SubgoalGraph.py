import numpy
import math
from numpy import where, dstack, diff, meshgrid
from numpy.linalg import norm

class SubgoalGraph:

    def __init__(self):
        self.node_list = []
        self.edges = []
        self.C = []
        self.sg_cand = []
        self.start = []
        self.sthresh = 0.1
        self.vmax = 5.0
        self._pred_traj = lambda x: x
        self.pl = 0
        self.sg_tol = 0.5
        self.fig = 12
        self.n_pred = 7
        self.goal = []
        self.startid = 0
        self.nodes_expanded = 0
        self.solution_depth = 0
        self.path_time = 0
        self.path_list = []
        self.dcost_thresh = 5.0
        self.nextid = 1
        self.use_edges = False
        self.open_list = []
        self.minrad = 10.0

    
    def set_function(self, pt_func):
        self._pred_traj = pt_func

    def get_obs(self, obsdata):
        n = int(max(obsdata['num'])) + 1
        C = []
        for i in range(0,n):
            idx = numpy.where(obsdata['num'] == i)[0]
            ci = {}
            ci['verts'] = numpy.array([obsdata['xpos'][idx], obsdata['ypos'][idx]]).T
            ci['mean'] = numpy.mean(ci['verts'], axis=0)
            # ci['mean'] = numpy.array([[numpy.mean(ci['verts'][0,:])],[numpy.mean(ci['verts'][1,:])]])
            ci['sgp'] = []
            for j in range(len(ci['verts'])):
                # print(ci['verts'][j,:])
                # print(ci['mean'])
                vp = ci['verts'][j,:] - ci['mean']
                vpd = numpy.linalg.norm(vp)
                vpe = vp/vpd
                sgx = vpe * (vpd + self.sg_tol) + ci['mean']
                # print("appending sg cand")
                # print(vp)
                # print(sgx)
                ci['sgp'].append(sgx)
            #ci['sgp'] = numpy.array(ci['sgp'])
            if len(ci['verts']) > 0:
                C.append(ci)
        self.C = C.copy()

    def a_star_search(self, goal):
        print("starting search...")
        print("subgoals:")
        for sgi in self.sg_cand:
            print(sgi)
        self.nodes_expanded = 0
        self.open_list = []
        # self.closed_list = []
        self.goal = goal
        self.start['fcost'] = self._compute_cost(self.start, self.goal)
        self.goal['fcost'] = self.goal['gcost'] + self._compute_cost(self.start, self.goal)

        # open_list contains actual nodes/references to nodes
        self.open_list.append(goal.copy())
        status = 0
        done = False

        while ((len(self.open_list) > 0) & (not done)):
            # sort open list based on fcost
            self.open_list = sorted(self.open_list, key = lambda i: i['fcost'])
            print("open list",[(il['id'],il['fcost']) for il in self.open_list])

            # get the node with the lowest cost
            node = self.open_list[0]
            print("opening:",node['id'], node['fcost'])

            # then remove that entry from the open list
            self.open_list = self.open_list[1:]

            # check to see if this node is the start:
            if node['isstart']:
                status = True
                done = True
                print('Found the start!')

            # expand adjacent nodes
            neighbor_list = self._get_adjacent_bw(node)
            #print([n['id'] for n in neighbor_list], "node", node['id'])
            # keep track of the number of expanded nodes
            self.nodes_expanded += len(neighbor_list)
            # check neighbors, in order from lowest to highest cost
            for neighbor in neighbor_list:
                # print("checking:", neighbor)
                # if expanded neighbor cost is better, update that sgcand cost:
                sgcand, idx = self._get_node(neighbor['id'], self.sg_cand)
                if sgcand['gcost'] >= neighbor['gcost']:
                    # print("connecting")
                    print("updating sg", neighbor['id'], "next from", neighbor['next'], "to", node['id'])
                    neighbor['next'] = node['id']
                    self.sg_cand[idx]['next'] = node['id']
                    # also update the sgcand state:
                    # self.sg_cand[idx]['state'] = neighbor['state']
                    # self.sg_cand[idx]['fcost'] = neighbor['fcost']
                    # self.sg_cand[idx]['gcost'] = neighbor['gcost']
                    self.sg_cand[idx] = neighbor.copy()

                    # keep track of which neighbors are open:
                    neighbor_is_open = [ol['id']==neighbor['id'] for ol in self.open_list]
                    # if neighbor is already in the open list, update the cost
                    if any(neighbor_is_open):
                        idx = numpy.where(neighbor_is_open)[0][0]
                        self.open_list[idx] = neighbor.copy()
                    else:
                        self.open_list.append(neighbor.copy())
        return status


    def _get_adjacent_bw(self, node):
        # find subgoals
        # first consider corners
        # compute heuristic costs, i.e. based on distance
        hlist = []
        for sg in self.sg_cand:
            if (sg['id'] != node['id']) & (not sg['edge']):
                sgtemp = sg.copy()
                sgtemp['state'] = numpy.stack([sg['pos'], (sg['pos'] - node['pos'])/norm(node['pos'] - sg['pos'])])
                #new_h = numpy.array([sg['id'], node['gcost']+self._compute_cost(node,sg)+self._compute_cost(sg,self.start)])
                new_h = {
                    'id': sg['id'],
                    'cost': node['gcost']+self._compute_cost(node,sgtemp)+self._compute_cost(sg,self.start)
                }
                hlist.append(new_h)
                # sg['state'] = []
        # sort the list based on estimated cost
        # hlist = numpy.array(hlist)
        # print("hlist", hlist)
        # hlist = hlist[hlist[:,1].argsort(),:]
        hlist = sorted(hlist, key = lambda i: i['cost'])
        nlist = []
        # nl = self.n_pred[self.n_pred.argsort()[:,len(self.sg_cand)],:]
        nl = min(self.n_pred, len(self.sg_cand))
        n = 0
        i = 0
        hlast = 0
        while ((n < nl) & (i < len(hlist))):
            dcost = abs(hlist[i]['cost'] - hlast)
            if ((n < 3) | (dcost < self.dcost_thresh)):
                sg, idx = self._get_node(hlist[i]['id'], self.sg_cand)
                sg = sg.copy()
                if ((sg['id'] != self.startid) & (sg['next'] == node['id']) & (numpy.isfinite(sg['gcost']))):
                    sg['hcost'] = self._compute_cost(sg, self.start)
                    sg['fcost'] = sg['gcost'] + sg['hcost']
                    nlist.append(sg)
                    hlast = sg['fcost']
                    n += 1
                else:
                    # const = any(sg['obstructed']==node['id'])
                    const = False
                    print("testing traj from", sg['pos'], "to", node['pos'], node['state'])
                    if not const:
                        # print("pos", sg['pos'], "state", node['state'])
                        try:
                            traj = self._pred_traj(sg['pos'], numpy.hstack([node['pos'], node['state']]))
                        except:
                            print("pred traj error:")
                            print(node)
                            raise
                        # print("traj",traj)
                        const = (-traj['tt'][0] > 10000)
                        if const:
                            sg_obs = numpy.unique(numpy.vstack([sg['obstructed'], node['id']]))
                            self.sg_cand[sg['id']]['obstructed'] = sg_obs
                    if not const:
                        # btrajx = [traj['x'][:,1], traj['x'][:,1] - 0.5*traj['v'][:,1]]
                        const = [] #self.intersects(btrajx, self.C)
                    if not const:
                        const, ob = intersects(traj['x'], self.C)
                    if not const:
                        h = -traj['tt'][0]
                        sg['state'] = traj['v'][0,:]
                        sg['gcost'] = h + node['gcost']
                        sg['hcost'] = norm(self.start['pos'] - sg['pos'])/self.vmax
                        sg['fcost'] = sg['gcost'] + sg['hcost']
                        nlist.append(sg)
                        n += 1
                        hlast = sg['fcost']
                        print(" => possible")
                    else:
                        print(" => obstructed")
            i += 1
        if ((n < nl) & self.use_edges & (not node['edge'])):
            esgs = []
            for i in range(self.edges):
                esg = edge_subgoal(node, self.edges[i]).T
                if len(esg) > 0:
                    if len(esg)==1:
                        esgs.append(esg[0].T)
                    elif len(esg) == 2:
                        esgs.append(esg[0].T)
                        esgs.append(esg[1].T)
            esgs = numpy.unique(esgs, axis=0)
            if len(esgs) > 0:
                for i in range(len(esgs[:,0])):
                    const = 0
                    sgpos = esgs[i,:].T
                    const = (norm(sgpos - node['pos']) < 0.1)
                    if not const:
                        traj = self._pred_traj(sgpos, numpy.hstack([node['pos'], node['state']]))
                        const = ((-traj['tt'][0] > 10000) | (-traj['tt'][0] == 0))
                    if not const:
                        h = -traj['tt'][0]
                        sg = self._create_node(sgpos, self.nextid)
                        self.nextid = self.nextid + 1
                        sg['state'] = traj['v'][0,:]
                        sg['gcost'] = h + node['gcost']
                        sg['hcost'] = norm(self.start['pos'] - sg['pos'])/self.vmax
                        sg['fcost'] = sg['gcost'] + sg['hcost']
                        sg['edge'] = True
                        self.add_node(sg)
                        self.sg_cand.append(sg)
                        nlist.append(sg)
                        n += 1

        return nlist


    def _create_node(self, pos, id=None):
        node = {}
        node['pos'] = pos
        node['state'] = []
        node['fcost'] = numpy.inf
        node['gcost'] = numpy.inf
        node['hcost'] = numpy.inf
        node['neighbors'] = []
        node['opened'] = False
        node['closed'] = False
        node['next'] = 0
        node['obstructed'] = numpy.array([])
        node['edge'] = 0
        node['isstart'] = False
        if id is not None:
            node['id'] = id
        else:
            node['id'] = self.nextid
            self.nextid += 1
        return node


    def _compute_cost(self, nodeend, nodestart):
        d = nodeend['pos'] - nodestart['pos']
        cost = numpy.linalg.norm(d)/self.vmax
        return cost


    def _get_node(self, id, nlist):
        node = []
        if id==0:
            node = self.goal
            ni = 0
        else:
            nid = numpy.array([ni['id'] for ni in nlist])
            ni = numpy.where(nid==id)[0][0]
            node = nlist[ni]
        return node, ni


    def _update_cost(self, id, ncost, nlist):
        for i in range(len(nlist)):
            if nlist[i]['id']==id:
                if ncost < nlist[i]['fcost']:
                    nlist[i]['fcost'] = ncost
        return nlist

    def _get_sgcand(self):
        self.sg_cand = []
        self.edges = []
        id = 1
        for ci in self.C:
            for j in range(ci['verts'].shape[0]):
                sg = self._create_node(ci['sgp'][j])
                id += 1
                self.sg_cand.append(sg)

    def _set_start(self, pos):
        self.startid = self.nextid
        self.start = self._create_node(pos)
        self.start['isstart'] = True
        self.sg_cand.append(self.start)

    def get_path(self):
        node = self.start
        nid = node['id']
        path_list = []
        while nid is not 0:
            # print(nid)
            path_list.append(node)
            nid = node['next']
            node, idx = self._get_node(nid, self.sg_cand)
        path_list.append(self._get_node(0, self.sg_cand)[0])
        return path_list


def generate_obstacles(n=40, r=[1,7]):
    from numpy.random import randn, rand, ranf
    from matplotlib.patches import Polygon
    import matplotlib.path as path
    # generate random obstacle field

    # n = 40
    # r = [1, 7]
    rx = [0, 100]
    ry = [0, 50]

    plist = []
    points = []
    while len(plist) < n:
        w = ranf()*(r[1] - r[0]) + r[0]
        h = ranf()*(r[1] - r[0]) + r[0]
        x = ranf()*(rx[1] - rx[0]) + rx[0]
        y = ranf()*(ry[1] - ry[0]) + ry[0]
        th = ranf()*(numpy.pi/2)
        Rth = numpy.array([[numpy.cos(th), numpy.sin(th)],[-numpy.sin(th), numpy.cos(th)]])
        p1 = numpy.array([[x],[y]]) + numpy.dot(Rth, numpy.array([[w/2],[h/2]]))
        p2 = numpy.array([[x],[y]]) + numpy.dot(Rth, numpy.array([[-w/2],[h/2]]))
        p3 = numpy.array([[x],[y]]) + numpy.dot(Rth, numpy.array([[-w/2],[-h/2]]))
        p4 = numpy.array([[x],[y]]) + numpy.dot(Rth, numpy.array([[w/2],[-h/2]]))

        pl = numpy.array([p1.T, p2.T, p3.T, p4.T]).reshape(4,2)
        pg = Polygon(pl, closed=True)

        pa = path.Path(pl)
        # check if this new polygon intersects any existing polygons
        # if any points in pl fall inside any existing polygons
        if len(points) > 0:
            isect = any(pa.contains_points(points))
            for pli in plist:
                plth = path.Path(pli)
                isect = isect | any(plth.contains_points(pl))
        else:
            isect = False
        # for pi in pl:
        #     isect = isect | pa.contains_points(pi)

        # or any exsiting polygon points fall inside pl
        if not isect:
            plist.append(pl)
            points.append(p1.reshape(2,))
            points.append(p2.reshape(2,))
            points.append(p3.reshape(2,))
            points.append(p4.reshape(2,))
            
    return plist

def is_between(line1, line2, pt):
    # parallel unit vector
    e11 = (line1[[0,2]] - line1[[1,3]])/numpy.linalg.norm(line1[[0,2]] - line1[[1,3]])
    e11 = e11.reshape([2,1])
    # perpendicular unit vector
    e12 = numpy.dot(numpy.array([[0,1],[-1,0]]), e11)
    # vector from point on line1 to point on line2
    s = line2[[0,2]].reshape([2,1]) - line1[[0,2]].reshape([2,1])
    # vector from point on line1 to point
    sa = pt - line1[[0,2]].reshape([1,2]).T
    d = numpy.dot(s.T, e12)[0,0]
    da = numpy.dot(sa.T, e12)[0,0]
    g1 = numpy.absolute(d) - numpy.absolute(da)
    between = (g1 > 0.1) & (numpy.sign(da) == numpy.sign(d))
    return between


def is_inside(pt, C, outer=False):
    obsnum = -1
    inside = False
    n = len(C)
    for i in range(n):
        if outer:
            xp = C[i]['sgp'][0,:]
            yp = C[0]['sgp'][1,:]
        else:
            xp = C[i]['verts'][0,:]
            yp = C[0]['verts'][1,:]

        line1 = numpy.array([xp[0], xp[1], yp[0], yp[1]])
        line2 = numpy.array([xp[2], xp[3], yp[2], yp[3]])
        line3 = numpy.array([xp[0], xp[3], yp[0], yp[3]])
        line4 = numpy.array([xp[1], xp[2], yp[1], yp[2]])

        ins = is_between(line1, line2, pt) & is_between(line3, line4, pt)
        if (ins):
            obsnum = i
        inside = inside | ins

    return inside, obsnum


def intersects(xp, C):
    # assemble list of all line segments in obstructions
    cl = []
    for i in range(len(C)):
        ls = numpy.hstack([C[i]['verts'][0,:], C[i]['verts'][1,:]])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][1,:], C[i]['verts'][2,:]])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][2,:], C[i]['verts'][3,:]])
        cl.append(ls)
        ls = numpy.hstack([C[i]['verts'][3,:], C[i]['verts'][0,:]])
        cl.append(ls)

    cl = numpy.array(cl)

    # then check to see if the trajectory intersects any of them
    # print(xp.shape)
    # xp = xp.T
    
    xln = numpy.hstack([xp[:-1,:], xp[1:,:]]).reshape([-1,4])

    # print(xln.shape)
    # print(cl)
    try:
        out = line_segment_intersect(cl, xln)
    except:
        print("error", cl, C)
        raise
    obs_int = out['intAdjacencyMatrix'].T.any(axis=0)
    trj_int = out['intAdjacencyMatrix'].T.any(axis=1)
    
    # print(out['intAdjacencyMatrix'].T.shape, numpy.sum(out['intAdjacencyMatrix']))

    obs = numpy.floor(numpy.where(obs_int)[0]/(4 * len(C)))
    cst = obs_int.any()
    seg = numpy.where(trj_int)[0]
    # seg = trj_int

    return cst, obs

def line_segment_intersect(xy1, xy2):

    # print("input shapes:", xy1.shape, xy2.shape)

    n_rows_1, n_cols_1 = xy1.shape
    n_rows_2, n_cols_2 = xy2.shape

    sh = (n_rows_1, n_rows_2)

    if (n_cols_1 != 4) | (n_cols_2 != 4):
        print('Arguments must be a Nx4 matrices.')
        print(n_cols_1, n_cols_2)

    x1 = numpy.tile(xy1[:,0:1],(1,n_rows_2)) #.reshape(sh)
    x2 = numpy.tile(xy1[:,2:3],(1,n_rows_2)) #.reshape(sh)
    y1 = numpy.tile(xy1[:,1:2],(1,n_rows_2)) #.reshape(sh)
    y2 = numpy.tile(xy1[:,3:4],(1,n_rows_2)) #.reshape(sh)

    # print("shapes: ", xy1.shape, x1.shape)

    xy2 = xy2.T

    # print("line segment input shapes", xy1.shape, xy2.shape)

    x3 = numpy.tile(xy2[0:1,:],(n_rows_1,1)) #.reshape(sh)
    x4 = numpy.tile(xy2[2:3,:],(n_rows_1,1)) #.reshape(sh)
    y3 = numpy.tile(xy2[1:2,:],(n_rows_1,1)) #.reshape(sh)
    y4 = numpy.tile(xy2[3:4,:],(n_rows_1,1)) #.reshape(sh)

    # print(x1.shape, x2.shape, x3.shape, x4.shape)
    # print(y1.shape, y2.shape, y3.shape, y4.shape)

    x4_x3 = x4 - x3
    y1_y3 = y1 - y3
    y4_y3 = y4 - y3
    x1_x3 = x1 - x3
    x2_x1 = x2 - x1
    y2_y1 = y2 - y1

    numerator_a = numpy.multiply(x4_x3, y1_y3) - numpy.multiply(y4_y3, x1_x3)
    numerator_b = numpy.multiply(x2_x1, y1_y3) - numpy.multiply(y2_y1, x1_x3)
    denominator = numpy.multiply(y4_y3, x2_x1) - numpy.multiply(x4_x3, y2_y1)

    # print(numerator_a.shape, numerator_b.shape, denominator.shape)

    u_a = numpy.divide(numerator_a, denominator)
    u_b = numpy.divide(numerator_b, denominator)

    # print(u_a.shape, u_b.shape)

    int_x = x1 + numpy.multiply(x2_x1, u_a)
    int_y = y1 + numpy.multiply(y2_y1, u_a)
    int_b = numpy.logical_and.reduce([(u_a >= 0), (u_a <= 1), (u_b >= 0), (u_b <= 1)])
    
    # print(u_a.shape, u_b.shape, int_b.shape, x4_x3.shape, y1_y3.shape)
    
    # par_b = (denominator == 0)
    # coinc_b = (numerator_a == 0) & (numerator_b == 0) & (par_b)

    out = {}
    out['intAdjacencyMatrix'] = int_b
    out['intMatrixX'] = int_x * int_b
    out['intMatrixY'] = int_y * int_b

    return out